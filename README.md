# XYZ Forum

a forum application. this application follows Domain Driven Design as it brings more testability and separate the application from the business logic. 

## Requirements
* PHP >= 8.0 
* mysql
* node >= 14 
* composer >= 2.1.8
* npm >= 6.14.17


## API Installation

2. run `cd api` 
3. run `composer install`
3. run ` cp .env.example .env`
4. update database credentials in .env
````
DB_CONNECTION=
DB_HOST=
DB_PORT=
DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=
````
5. update mail server credentials in .env (need for custom command)
````
MAIL_MAILER=
MAIL_HOST=
MAIL_PORT=
MAIL_USERNAME=
MAIL_PASSWORD=
MAIL_ENCRYPTION=
````
6. run `php artisan migrate` to migrate the data base
6. run `php artisan db:seed` to seed sample data to the database
7. run `php artisan test` to test the
7. run `php run ser` to run the dev server




* please refer to fourm.postman_collection.json postman collection for api documentation
* add `0 0 * * * path/to/project/folder/ php artisan email:daily-pending-posts` to crontab to init daily pending posts email to admin. `ADMIN_EMAIL` need to be added to .env
* admin account
    email = "admin@admin.com"
    password = "password"

# App Installation

1. run `cd app`
2. run `npm install`
3. run ` cp .env.example .env`
4. update `VUE_APP_API_URL` with API url in .env
3. run `npm run serve` to run dev server
