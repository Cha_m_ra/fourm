import axios from "axios";

const state = {
    user: null,
    token: ""
};

const getters = {
    isAuthenticated: (state) => !!state.user,
    StateUser: (state) => state.user,
    userToken: (state) => state.token
};

const actions = {
    async Register({
        commit
    }, form) {
        const response = await axios.post('/register', form)

        commit('setUser', response.data.user)
        commit('setToken', response.data.token)
    },

    async Login({
        commit
    }, user) {
        const response = await axios.post("/login", user);

        commit('setUser', response.data.user)
        commit('setToken', response.data.token)
    },

    async Logout({
        commit
    }) {
        commit("logout");
    },
};

const mutations = {
    setUser(state, userData) {
        state.user = userData;
    },
    setToken(state, token) {
        state.token = token;
    },
    logout(state) {
        state.user = null;
        state.token = null;
    }
};

export default {
    state,
    getters,
    actions,
    mutations,
};