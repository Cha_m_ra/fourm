import '@babel/polyfill'
import 'mutationobserver-shim'
import Vue from 'vue'
import './plugins/bootstrap-vue'
import App from './App.vue'
import store from "./store";
import router from './router'
import axios from 'axios';
import VueToast from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-sugar.css';
import mixin from './mixin';


axios.interceptors.response.use(undefined, function (error) {
  if (error) {
    const originalRequest = error.config;
    if (error.response.status === 401 && !originalRequest._retry) {
      originalRequest._retry = true;
      store.dispatch("logoit");
      return router.push("/login");
    }
  }
});

axios.interceptors.request.use(config => {  
  config.headers["Authorization"] = `Bearer ${store.getters.userToken}`;
  return config;
});

Vue.config.productionTip = false

Vue.use(VueToast);

Vue.mixin(mixin)

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')
