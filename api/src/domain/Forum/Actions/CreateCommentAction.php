<?php

namespace Domain\Forum\Actions;

use App\Models\Post;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class CreateCommentAction {

    public function __invoke(
        Post $post,
        User $user,
        array $commentData
    )
    {
        try {

            $commentData['user_id'] = $user->id;

            DB::beginTransaction();

            $comment = $post->comments()->create($commentData);

            DB::commit();

            return $comment;
            
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }

    }
}