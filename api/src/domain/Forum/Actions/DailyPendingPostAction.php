<?php

namespace Domain\Forum\Actions;

use App\Mail\DailyPendingPostEmail;
use App\Models\Post;
use Illuminate\Support\Facades\Mail;

class DailyPendingPostAction
{


    public function __invoke()
    {
        $posts = Post::whereDate('created_at', '=',date('Y-m-d'))
                ->whereStatus(Post::PENDING)
                ->with('user')
                ->get();

        Mail::to(env('ADMIN_EMAIL'))
            ->send(
                new DailyPendingPostEmail($posts)
            );

    }
}
