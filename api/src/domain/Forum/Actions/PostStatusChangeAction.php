<?php

namespace Domain\Forum\Actions;

use App\Models\Post;
use App\Models\User;
use Domain\Forum\Exceptions\NotAllowedException;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\DB;

class PostStatusChangeAction {

    public function __invoke(
        Authenticatable | User $user,
        Post $post, 
        array $data
    ) : void
    {
        try {
            
            if (!$user->isAdmin()) {
                throw new NotAllowedException();
            }
            
            DB::beginTransaction();

            $post->status = $data['status'];
            $post->status_changed_by = $user->id;
            $post->status_changed_at = now();
            $post->save();

            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }
}