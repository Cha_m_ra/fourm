<?php

namespace Domain\Forum\Actions;

use App\Models\Post;

class SearchPostAction
{

    public function __invoke(
        array|null $searchData
    ) {
        try {
            $search = isset($searchData['search']) ? $searchData['search'] : null;
            $onlyMe = isset($searchData['only_me']) ? $searchData['only_me'] : null;

            return Post::when($search, function ($query) use ($search) {
                    $query->where('title', 'like', "%" . $search . "%")
                        ->orWhere('description', 'like', "%" . $search . "%")
                        ->orWhereHas('user', function ($query) use ($search) {
                            $query->where('name', 'like', $search);
                        });
                })
                ->when($onlyMe == 1, function ($query) {
                    $query->whereUserId(auth()->user()->id);
                })
                ->where(function ($query) use ($onlyMe) {

                    if (!auth()->user()->is_admin && $onlyMe == 0) {
                        $query->whereStatus(Post::APPROVED);
                    }
                })
                ->with(['user'])
                ->paginate();
        } catch (\Exception $e) {

            throw $e;
        }
    }
}
