<?php

namespace Domain\Forum\Actions;

use App\Models\Post;

class PostCommentAction
{

    public function __invoke(
        Post $post,
    ) {
        try {
            return $post->comments;
            
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
