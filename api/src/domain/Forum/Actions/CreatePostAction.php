<?php

namespace Domain\Forum\Actions;

use App\Models\Post;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\Auth\Authenticatable;

class CreatePostAction
{


    public function __invoke(Authenticatable | User $user, array $postData): Post
    {
        try {
            DB::beginTransaction();

            $post =  $user->posts()->create($postData);

            if ($user->isAdmin()) {

                $post->status = Post::APPROVED;
                $post->status_changed_by = $user->id;
                $post->status_changed_at = now();

                $post->save();
            }

            DB::commit();

            return $post->load(['user']);
        } catch (\Exception $e) {
            DB::rollBack();

            throw $e;
        }
    }
}
