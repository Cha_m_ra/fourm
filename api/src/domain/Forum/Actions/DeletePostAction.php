<?php

namespace Domain\Forum\Actions;

use App\Models\Post;
use App\Models\User;
use Domain\Forum\Exceptions\NotAllowedException;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\Auth\Authenticatable;

class DeletePostAction
{


    public function __invoke(Authenticatable | User $user, Post $post) 
    {
        try {
            DB::beginTransaction();

            if($user->id !== $post->user_id) {
                throw new NotAllowedException();
            }

            $post->delete();

            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();

            throw $e;
        }
    }
}
