<?php

namespace Domain\User\Actions;

use App\Models\User;
use Domain\User\Exceptions\LoginFailException;
use Illuminate\Support\Facades\Auth;

class LoginUserAction {

    public function __invoke(array $loginData) : array
    {        
        if (!Auth::attempt($loginData)) {
            throw new LoginFailException("Error Processing Request", 1);
        }

        $user = User::whereEmail($loginData['email'])->first();

        return [
            'user' => $user,
            'token' => $user->getToken(),
        ];
    }
}