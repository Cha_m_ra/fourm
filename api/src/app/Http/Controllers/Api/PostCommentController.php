<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\PostCommentRequest;
use App\Models\Post;
use Domain\Forum\Actions\CreateCommentAction;
use Domain\Forum\Actions\PostCommentAction;
use Illuminate\Http\Response;

class PostCommentController extends Controller
{
    public function index(
        Post $post,
        PostCommentAction $postCommentAction
    )
    {
        try {

            return response()->json(
                $postCommentAction(
                    post: $post
                ),
                Response::HTTP_OK
            );
        } catch (\Throwable $th) {

            report($th);
            return response()->json('Failed to add comment', Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }


    public function store(
        Post $post,
        PostCommentRequest $request,
        CreateCommentAction $createCommentAction
    ) {
        try {

            return response()->json(
                $createCommentAction(
                    post: $post,
                    user: auth()->user(),
                    commentData: $request->validated()
                ),
                Response::HTTP_OK
            );
            
        } catch (\Throwable $th) {

            report($th);
            return response()->json('Failed to add comment', Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
