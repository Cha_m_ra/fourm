<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\PostRequest;
use App\Http\Requests\PostStatusRequest;
use App\Models\Post;
use Domain\Forum\Actions\CreatePostAction;
use Domain\Forum\Actions\DeletePostAction;
use Domain\Forum\Actions\PostStatusChangeAction;
use Domain\Forum\Actions\SearchPostAction;
use Domain\Forum\Exceptions\NotAllowedException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(
        Request $request,
        SearchPostAction $searchPostAction
    )
    {
        try {
            
            return response()->json(
                $searchPostAction($request->only(['search', 'only_me'])),
                Response::HTTP_OK
            );           

        } catch (\Throwable $th) {
            report($th);
            return response()->json('Failed to search post', Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  CreatePostAction  $createPostAction
     * @return \Illuminate\Http\Response
     */
    public function store(
        PostRequest $request,
        CreatePostAction $createPostAction
    ) {
        try {
            return response()->json(
                $createPostAction(user: auth()->user(), postData: $request->validated()),
                Response::HTTP_OK
            );
        } catch (\Exception $th) {
            report($th);
            return response()->json('Failed to create post', Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Change post status
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  PostStatusChangeAction  $postStatusChangeAction
     * @return \Illuminate\Http\Response
     */
    public function status(
        PostStatusRequest $request,
        Post $post,
        PostStatusChangeAction $postStatusChangeAction
    ) {
        try {

            $postStatusChangeAction(
                user: auth()->user(),
                post : $post,
                data: $request->validated()
            );

            return response()->json('Status changed successful', Response::HTTP_OK);
            
        } catch (NotAllowedException $th) {

            return response()->json('Unauthorize', Response::HTTP_UNAUTHORIZED);
        } catch (\Throwable $th) {

            report($th);
            return response()->json('Failed to change posts status', Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Change post status
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  PostStatusChangeAction  $postStatusChangeAction
     * @return \Illuminate\Http\Response
     */
    public function destroy(
        Post $post,
        DeletePostAction $deletePostAction
    )
    {
        try {

            $deletePostAction(
                user: auth()->user(),
                post: $post
            );

            return response()->json('Post deleted successful', Response::HTTP_OK);
        } catch (NotAllowedException $th) {

            return response()->json('Unauthorize', Response::HTTP_UNAUTHORIZED);
        } catch (\Throwable $th) {

            report($th);
            return response()->json('Failed to change posts status', Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
