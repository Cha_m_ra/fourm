<?php

namespace App\Console\Commands;

use Domain\Forum\Actions\DailyPendingPostAction;
use Illuminate\Console\Command;

class SendDailyPendingPostsEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:daily-pending-posts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(
        DailyPendingPostAction $dailyPendingPostAction
    )
    {
        $dailyPendingPostAction();

        return Command::SUCCESS;
    }
}
