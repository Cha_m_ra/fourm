<?php

namespace Tests\Feature\Domain\Forum\Actions;

use App\Models\Post;
use App\Models\User;
use Domain\Forum\Actions\PostStatusChangeAction;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PostStatusChangeActionTest extends TestCase
{
    use RefreshDatabase;
    
    public function test_post_can_be_approved_by_admin_user()
    {
        $post = Post::factory()->create();
        $user = User::factory()->create(['is_admin' => true]);
        $data = [
            'status' => Post::APPROVED,
        ];

        app(PostStatusChangeAction::class)($user, $post, $data);

        $this->assertTrue($post->refresh()->status === Post::APPROVED);
    }

    public function test_post_can_be_reject_by_admin_user()
    {
        $post = Post::factory()->create();
        $user = User::factory()->create(['is_admin' => true]);
        $data = [
            'status' => Post::REJECT,
        ];

        app(PostStatusChangeAction::class)($user, $post, $data);

        $this->assertTrue($post->refresh()->status === Post::REJECT);
    }
}