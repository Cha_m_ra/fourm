<?php

namespace Tests\Feature\Domain\Forum\Actions;

use App\Models\Post;
use App\Models\User;
use Domain\Forum\Actions\DeletePostAction;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class DeletePostActionTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_delete_post_action()
    {
        $user = User::factory()->create();
        $post = Post::factory()->create(['user_id' => $user->id]);


        app(DeletePostAction::class)($user, $post);

        $this->assertDatabaseMissing('posts', $post->only(['id','title']));
    }
}
