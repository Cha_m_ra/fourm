<?php

namespace Tests\Feature\Domain\Forum\Actions;

use App\Models\Post;
use App\Models\User;
use Domain\Forum\Actions\SearchPostAction;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class SearchPostActionTest  extends TestCase {

    use RefreshDatabase;
    
    public function test_user_can_search_post()
    {
        $user = new User([
            'is_admin' => true
        ]);

        $this->be($user);
        
        $title = "New Post";

        Post::factory()->create([
            'title' => $title,
            'status' => Post::APPROVED
        ]);

        $posts = app(SearchPostAction::class)(['search' => $title]);

        $this->assertTrue($posts->toArray()['data'][0]['title'] === $title);
    }
    
}