<?php

namespace Tests\Feature\Domain\Forum\Actions;

use App\Models\Product;
use App\Models\User;
use Domain\Forum\Actions\CreatePostAction;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CreatePostTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_create_post_action()
    {
        $user = User::factory()->create();

        $postData = [
            'title' => 'new product question',
            'description' => 'description',
        ];

        app(CreatePostAction::class)($user, $postData);

        $this->assertDatabaseHas('posts', $postData);
    }
}
