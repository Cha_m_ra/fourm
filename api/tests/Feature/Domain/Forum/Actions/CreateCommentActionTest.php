<?php

namespace Tests\Feature\Domain\Forum\Actions;

use App\Models\Post;
use App\Models\User;
use Domain\Forum\Actions\CreateCommentAction;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CreateCommentActionTest extends TestCase
{
    use RefreshDatabase;

    public function test_user_can_comment_on_post()
    {
        $post = Post::factory()->create();
        $user = User::factory()->create();

        $commentData = [
            'content' => fake()->text()
        ];

        app(CreateCommentAction::class)($post, $user, $commentData);

        $this->assertDatabaseHas('comments', $commentData);
    }
}
