<x-mail::message>
# Daily Peding Posts

Here are the newly created post form users

@foreach ($posts as $post)
<p>
    {{ $post->user->name }} posted {{ $post->title}} at {{ $post->created }} 
</p>
<hr>
@endforeach

<br>
Thanks,<br>
{{ config('app.name') }}
</x-mail::message>
