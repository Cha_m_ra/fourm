<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->renameColumn('is_approved', 'status');
            $table->renameColumn('approved_at', 'status_changed_at');
            $table->dropForeign(['approved_by']);
            $table->renameColumn('approved_by', 'status_changed_by');

            $table->foreign('status_changed_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->renameColumn('status', 'is_approved');
            $table->renameColumn('status_changed_at', 'approved_at');
            $table->dropForeign(['status_changed_by']);
            $table->renameColumn('status_changed_by', 'approved_by');

            $table->foreign('approved_by')->references('id')->on('users');
        });
    }
};
